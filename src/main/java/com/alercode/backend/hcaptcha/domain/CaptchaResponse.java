package com.alercode.backend.hcaptcha.domain;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CaptchaResponse implements Serializable {
    private boolean      success;
    @SerializedName("challenge_ts")
    private String       timestamp;
    private String       hostname;
    @SerializedName("error-codes")
    private List<String> errors;
}
