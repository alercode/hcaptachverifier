package com.alercode.backend.hcaptcha;

import com.alercode.backend.hcaptcha.api.CaptchaService;
import com.alercode.backend.hcaptcha.exception.*;
import io.reactivex.Completable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

public final class Captcha {
    private static Captcha        instance;
    private final  String         secret;
    private final  CaptchaService service;

    private Captcha(String secret) {
        this.secret = secret;
        service     = new Retrofit.Builder()
                .baseUrl("https://hcaptcha.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(CaptchaService.class);
    }

    public static Captcha getInstance() {
        return instance;
    }

    public static void init(String secret) {
        instance = new Captcha(secret);
    }

    public Completable verify(String token) {
        return service.verify(secret, token)
                .flatMapCompletable(captchaResponse -> {
                    if (captchaResponse.isSuccess()) {
                        return Completable.complete();
                    } else if (captchaResponse.getErrors() != null) {
                        return Completable.error(parseError(captchaResponse.getErrors()));
                    }
                    return Completable.error(new Exception("Unknown error"));
                });
    }

    private Exception parseError(List<String> errors) {
        if (errors.contains("missing-input-secret")) {
            return new MissingSecretException();
        } else if (errors.contains("invalid-input-secret")) {
            return new InvalidSecretException();
        } else if (errors.contains("missing-input-response")) {
            return new MissingTokenException();
        } else if (errors.contains("invalid-input-response")) {
            return new InvalidTokenException();
        } else if (errors.contains("bad-request")) {
            return new BadRequestException();
        } else if (errors.contains("invalid-or-already-seen-response")) {
            return new AlreadySeenException();
        } else if (errors.contains("sitekey-secret-mismatch")) {
            return new SiteKeyMismatchException();
        }
        return new Exception(String.join(",", errors));
    }
}
