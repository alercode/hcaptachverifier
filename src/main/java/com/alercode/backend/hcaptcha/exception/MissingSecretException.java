package com.alercode.backend.hcaptcha.exception;

public class MissingSecretException extends Exception {
    public MissingSecretException() {
        super("Your secret key is missing.");
    }
}
