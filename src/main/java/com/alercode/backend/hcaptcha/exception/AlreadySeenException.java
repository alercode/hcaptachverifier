package com.alercode.backend.hcaptcha.exception;

public class AlreadySeenException extends Exception {
    public AlreadySeenException() {
        super("The response parameter has already been checked, or has another issue.");
    }
}
