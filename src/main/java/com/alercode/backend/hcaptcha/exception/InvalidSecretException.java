package com.alercode.backend.hcaptcha.exception;


public class InvalidSecretException extends Exception {
    public InvalidSecretException() {
        super("Your secret key is invalid or malformed.");
    }
}
