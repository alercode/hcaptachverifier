package com.alercode.backend.hcaptcha.exception;

public class BadRequestException extends Exception {
    public BadRequestException() {
        super("The request is invalid or malformed.");
    }
}
