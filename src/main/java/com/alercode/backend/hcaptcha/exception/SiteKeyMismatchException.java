package com.alercode.backend.hcaptcha.exception;

public class SiteKeyMismatchException extends Exception {
    public SiteKeyMismatchException() {
        super("The site key is not registered with the provided secret.");
    }
}
