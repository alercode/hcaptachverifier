package com.alercode.backend.hcaptcha.exception;

public class MissingTokenException extends Exception {
    public MissingTokenException() {
        super("The response parameter (verification token) is missing.");
    }
}
