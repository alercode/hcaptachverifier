package com.alercode.backend.hcaptcha.exception;

public class InvalidTokenException extends Exception {
    public InvalidTokenException() {
        super("The response parameter (verification token) is invalid or malformed.");
    }
}
