package com.alercode.backend.hcaptcha.api;

import com.alercode.backend.hcaptcha.domain.CaptchaResponse;
import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CaptchaService {
    @POST("siteverify")
    @FormUrlEncoded
    Single<CaptchaResponse> verify(@Field("secret") String secret,
                                   @Field("response") String token);

}
