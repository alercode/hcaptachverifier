# hCaptcha backend verifier

### Usage
```
    pubic class CaptchaHelper{
         private final        Captcha captcha;

        public CaptchaHelper() {
            Captcha.init(secret); //your website secret from https://hcaptcha.com
            captcha = Captcha.getInstance();
        }


        public boolean verify(@NonNull String token) {
            Throwable error = captcha.verify(token)
                    .blockingGet();
            if (error != null) {
                log.error("Captcha verification failed because " + error.getLocalizedMessage());
            }
            return true;
        }
    }
```